# DIY Health Management System || Senz Health #

DIY Health Management System is an innovative android application that expects interconnects private practising doctors, patients and laboratories with 100% confidentiality in the documents or messages relayed between each parties.

Unlike other mobile based health management applications, Senz Health guarantees that your data is not stored in an intermediate cloud storage

How is possible ?
-----------------

We use [SenZ](http://www.mysensors.info/) security platform which was developed by [SCoRe](http://www.scorelab.org/) 

It has simple innovative features such as encrypted messages and images.

SenZ handles end-to-end security (phone to phone) which ensures any issue with related to your privacy.

This is currently in a prototype level & requires features such as sending any document (.pdf etc) p2p. 



